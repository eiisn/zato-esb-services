# zato: ide-deploy=True

from datetime import datetime
from json import dumps, loads

from zato.server.service import Service


class PostProduct(Service):

    def handle(self):
        self.logger.info('Request: %s' % self.request.payload)
        self.logger.info('Request type: %s' % type(self.request.payload))
        service_response = {}

        
        payload = self.request.payload

        erp_post_product = self.outgoing.plain_http.get('ERP-Produit')
        erp_post_type_de_produit = self.outgoing.plain_http.get('ERP-Type-De-Produit')
        erp_post_unite_de_mesure = self.outgoing.plain_http.get('ERP-Unite-De-Mesure')
        erp_post_matiere_premiere = self.outgoing.plain_http.get('ERP-Matiere-Premiere')
        erp_post_formule = self.outgoing.plain_http.get('ERP-Formule')
        erp_post_composition = self.outgoing.plain_http.get('ERP-Composition')
        erp_post_gamme = self.outgoing.plain_http.get('ERP-Gamme')
        erp_post_detail_gamme = self.outgoing.plain_http.get('ERP-Detail-Gamme')
        erp_post_composition_gamme = self.outgoing.plain_http.get('ERP-Composition-Gamme')
        erp_post_nomenclature = self.outgoing.plain_http.get('ERP-Nomenclature')
        erp_post_detail_nomenclature = self.outgoing.plain_http.get('ERP-Detail-Nomenclature')

        erp_product_all = erp_post_product.conn.get(self.cid)
        erp_type_de_produit_all = erp_post_type_de_produit.conn.get(self.cid)
        erp_unite_de_mesure_all = erp_post_unite_de_mesure.conn.get(self.cid)
        erp_matiere_premiere_all = erp_post_matiere_premiere.conn.get(self.cid)
        erp_formule_all = erp_post_formule.conn.get(self.cid)
        erp_gamme_all = erp_post_gamme.conn.get(self.cid)
        erp_nomenclature_all = erp_post_nomenclature.conn.get(self.cid)

        erp_product_all = loads(erp_product_all.text)['data']
        erp_type_de_produit_all = loads(erp_type_de_produit_all.text)['data']
        erp_unite_de_mesure_all = loads(erp_unite_de_mesure_all.text)['data']
        erp_matiere_premiere_all = loads(erp_matiere_premiere_all.text)['data']
        erp_formule_all = loads(erp_formule_all.text)['data']
        erp_gamme_all = loads(erp_gamme_all.text)['data']
        erp_nomenclature_all = loads(erp_nomenclature_all.text)['data']

        self.logger.info('ESB: product_all %s' % erp_product_all)
        self.logger.info('ESB: type_de_produit_all %s' % erp_type_de_produit_all)
        self.logger.info('ESB: unite_de_mesure_all %s' % erp_unite_de_mesure_all)
        self.logger.info('ESB: matiere_premiere_all %s' % erp_matiere_premiere_all)
        self.logger.info('ESB: formule_all %s' % erp_formule_all)
        self.logger.info('ESB: gamme_all %s' % erp_gamme_all)
        self.logger.info('ESB: nomenclature_all %s' % erp_nomenclature_all)

        rd_product = payload['produit']
        rd_unite_de_mesure = payload['unite_de_mesure']
        rd_matiere_premiere = payload['matiere']
        rd_formule = payload['formule']
        rd_gamme = payload['gamme']
        rd_nomenclature = payload['nomenclature']

        self.logger.info('Request: rd_product %s' % rd_product)
        self.logger.info('Request: rd_unite_de_mesure %s' % rd_unite_de_mesure)
        self.logger.info('Request: rd_matiere_premiere %s' % rd_matiere_premiere)
        self.logger.info('Request: rd_formule %s' % rd_formule)
        self.logger.info('Request: rd_gamme %s' % rd_gamme)
        self.logger.info('Request: rd_nomenclature %s' % rd_nomenclature)

        # Unite de Mesure (list) :
        # list de toute les unités de mesure
        # Get de toute les unités de mesure, itéré sur les 2 listes et récupéré le uom de l'ERP
        # si concordance sinon créer une nouvelle dans l'ERP
        unite_de_mesure = [u for u in erp_unite_de_mesure_all]

        for uom in rd_unite_de_mesure:
            self.logger.info('unite de mesure : %s ' % str(uom))
            uom_exist = list(filter(lambda u: u['Nom'] == uom['nom'], erp_unite_de_mesure_all))
            if not uom_exist:
                self.logger.info('unite de mesure not exist : %s ' % str(uom))
                new_uom = erp_post_unite_de_mesure.conn.post(self.cid, dumps({
                    "Nom": uom['nom'],
                    "Catégorie": uom['categorie'],
                    "Symbole": uom['symbole'],
                    "Disponible": True
                }))
                self.logger.info('[new] unite de mesure : %s ' % str(new_uom.text))
                unite_de_mesure.append(loads(new_uom.text)["data"])
            else:
                unite_de_mesure.append(uom_exist[0])

        self.logger.info('[list] unite de mesure : %s ' % str(unite_de_mesure))

        # Matiere Premiere :
        # Produit (id), Fournisseur (id=None)
        matiere_premiere = [m for m in erp_matiere_premiere_all]

        for mp in rd_matiere_premiere:
            self.logger.info('matiere premiere : %s ' % str(mp))
            mp_exist = list(filter(lambda m: m['Nom'] == mp['nom'], erp_matiere_premiere_all))
            if not mp_exist:
                self.logger.info('matiere premiere not exist : %s ' % str(mp))
                uom = next(filter(lambda u: u['Symbole'] == mp['unite_de_mesure'], unite_de_mesure))
                new_mp = erp_post_matiere_premiere.conn.post(self.cid, dumps({
                    "Nom": mp['nom'],
                    "Type": mp['type'],
                    "Unité de Mesure": uom["id"]
                }))
                matiere_premiere.append(loads(new_mp.text)["data"])
                self.logger.info('[new] matiere premiere : %s ' % str(new_mp.text))
            else:
                matiere_premiere.append(mp_exist[0])
        
        self.logger.info('[list] matiere premiere : %s ' % str(matiere_premiere))

        # Formule :
        # Nom (str), Molecule (id Matiere Premiere = Produit)
        formule = None
        fm_exist = list(filter(lambda f: f['Nom'] == rd_formule['nom'], erp_formule_all))
        if not fm_exist:
            new_fm = erp_post_formule.conn.post(self.cid, dumps({
                "Nom": rd_formule['nom'],
                "Mémo": rd_formule['memo']
            }))
            formule = loads(new_fm.text)["data"]
            self.logger.info('[new] formule : %s ' % str(formule))
            for c in rd_formule['composition']:
                mp = next(filter(lambda m: m['Nom'] == c, matiere_premiere))
                new_comp = erp_post_composition.conn.post(self.cid, dumps({
                    "Matière Première": mp['id']
                }), {"pk": formule['id']})
                self.logger.info('[new] composition formule : %s ' % str(new_comp.text))
        else:
            formule = fm_exist[0]
        
        self.logger.info('[obj] formule : %s ' % str(formule))

        # Gamme :
        # Produit (id=le nouveau), Formule (id=la nouvelle)
        gamme = None
        g_exist = list(filter(lambda g: g['Titre'] == rd_gamme['nom'], erp_gamme_all))
        if not g_exist:
            new_g = erp_post_gamme.conn.post(self.cid, dumps({
                "Titre": rd_gamme['nom'],
                "Description": rd_gamme['description']
            }))
            gamme = loads(new_g.text)['data']
            self.logger.info('[new] gamme : %s ' % str(gamme))
            for detail in rd_gamme['gamme_detail']:
                new_detail = erp_post_detail_gamme.conn.post(self.cid, dumps({
                    "Ordre": int(detail['order']),
                    "Description Etape": detail['step']
                }), {"pk": gamme['id']})
                new_detail = loads(new_detail.text)['data']
                self.logger.info('[new] detail gamme : %s ' % str(new_detail))
                for compo in detail['item']:
                    mp = next(filter(lambda m: m['Nom'] == compo, matiere_premiere))
                    new_compo_gamme = erp_post_composition_gamme.conn.post(self.cid, dumps({
                        "Matière Première": mp['id']
                    }), {"pk": new_detail['id']})
                    self.logger.info('[new] composition gamme : %s ' % str(new_compo_gamme.text))
        else:
            gamme = g_exist[0]

        self.logger.info('[obj] gamme : %s ' % str(gamme))
        
        # Nomenclature :
        # Nom (str), Produit (id=le nouveau), Formule (id=la nouvelle)
        nomenclature = None
        n_exist = list(filter(lambda n: n['Titre'] == rd_nomenclature['nom'], erp_nomenclature_all))
        if not n_exist:
            new_n = erp_post_nomenclature.conn.post(self.cid, dumps({
                "Titre": rd_nomenclature['nom'],
                "Description": rd_nomenclature['description']
            }))
            nomenclature = loads(new_n.text)["data"]
            self.logger.info('[new] nomenclature : %s ' % str(nomenclature))
            for detail in rd_nomenclature['nomenclature_detail']:
                mp = next(filter(lambda m: m['Nom'] == detail['matiere'], matiere_premiere))
                uom = next(filter(lambda u: u['Symbole'] == detail['unite'], unite_de_mesure))
                new_detail = erp_post_detail_nomenclature.conn.post(self.cid, dumps({
                    "Ingrédient": mp['id'],
                    "Unité de Mesure": uom['id'],
                    "Quantité": detail['quantite']
                }), {"pk": nomenclature['id']})
                self.logger.info('[new] nomenclature : %s ' % str(new_detail.text))
        else:
            nomenclature = n_exist[0]

        self.logger.info('[obj] nomenclature : %s ' % str(nomenclature))

        # Produit :
        # Nom (str), prix de vente (float), prix de fabrication (float), 
        # categorie comptable (id=class 7), unité de mesure (id=recup precedement), 
        # achetable (bool), vendable (bool), productible (bool)

        product_type = None
        pt_exist = list(filter(lambda pt: pt['Nom'] == rd_product['type'], erp_type_de_produit_all))
        if not pt_exist:
            new_pt = erp_post_type_de_produit.conn.post(self.cid, dumps({
                "Nom": rd_product['type']
            }))
            product_type = loads(new_pt.text)['data']
        else:
            product_type = pt_exist[0]
        
        self.logger.info('[obj] product_type : %s ' % str(product_type))

        produit = None
        p_exist = list(filter(lambda p: p['Nom'] == rd_product['nom'], erp_product_all))
        if not p_exist:
            uom = next(filter(lambda u: u['Symbole'] == rd_product['unite_de_mesure'], unite_de_mesure))
            new_p = erp_post_product.conn.post(self.cid, dumps({
                "Nom": rd_product['nom'],
                "Formule": formule['id'],
                "Type": product_type['id'],
                "Gamme": gamme['id'],
                "Nomenclature": nomenclature['id'],
                "Unité de Mesure": uom['id'],
                "Productible": True
            }))
            produit = loads(new_p.text)["data"]
        else:
            produit = p_exist[0]

        self.logger.info('[obj] product : %s ' % str(produit))

        service_response = dumps({"data": "OK"})
        
        #self.logger.info('Exception - ERP GET connexion : %s' % str(e))
        #service_response = {"data": "KO", "error": str(e)}

        self.response.headers.update({"Access-Control-Allow-Origin": "*"})
        self.response.payload = service_response
