# zato: ide-deploy=True

from datetime import datetime
from json import dumps

from zato.server.service import Service


class PostCommande(Service):

    def handle(self):
        self.logger.info('Request: %s' % self.request.payload)
        self.logger.info('Request type: %s' % type(self.request.payload))
        service_response = {}

        try:
            # needed data : 
            # client id, adresse id, each product id

            erp_post_commande = self.outgoing.plain_http.get('ERP-Vente')
            erp_post_commande_detail = self.outgoing.plain_http.get('ERP-Vente-Detail')
            etp_post_livraison = self.outgoing.plain_http.get('ERP-Livraison')

            # Créer une Livraison : 
            # Référence (str), Retour (bool=False), 
            # date commande (date=today), date plannifié (data=today+7j), 
            # Client (id[int]), Adresse (id[int]), Dépôt départ (id[int]=Un au pif), Etat (id[int]=En préparation)

            # Créer une Vente :
            # Date (date=today), Monnaie (str="EUR"), Total Factur (float=somme de tout les produit * quantité), 
            # Informations (str), Client (id[int]), Livraison (id[int])

            # Pour chaque Produit :
            # Créer un Détail de vente : 
            # Id vente au dessus 
            # Produit (id[int]), Quantité (int), Prix total (prix produit * quantité)

            # response = erp_post_commande.conn.get(self.cid)
            # response = erp_post_commande_detail.conn.get(self.cid)
            response = {}
            self.logger.info('ERP connexion : %s' % response)
            service_response = response
        except Exception as e:
            self.logger.info('Exception - ERP GET connexion : %s' % e)

        self.response.headers.update({"Access-Control-Allow-Origin": "*"})
        self.response.payload = service_response
