# zato: ide-deploy=True

from datetime import datetime
from json import dumps

from zato.server.service import Service


class PostCreateClient(Service):

    def handle(self):
        self.logger.info('Request: %s' % self.request.payload)
        self.logger.info('Request type: %s' % type(self.request.payload))
        service_response = {}

        try:
            payload = self.request.payload

            erp_post_create_client = self.outgoing.plain_http.get('ERP-Client')
            erp_post_adresse = self.outgoing.plain_http.get('ERP-Adresse')

            # Adresse :
            # Active (bool), Facturation (bool), Livraison (bool), 
            # Nom Batîment (str, null), Rue (str), Code Postale (str), 
            # Ville (str), Pays (str)

            # Client : 
            # Nom (str), Prenom (str), Dette (float=0), Restant Dû (float=0), Adresse (id=au dessus)

            response = erp_post_create_client.conn.get(self.cid)
            self.logger.info('ERP connexion : %s' % response)
            self.logger.info('ERP connexion dict : %s' % response.__dict__)
            self.logger.info('ERP connexion text : %s' % response.text)
            service_response = response.text
        except Exception as e:
            self.logger.info('Exception - ERP GET connexion : %s' % e)
        
        self.response.headers.update({"Access-Control-Allow-Origin": "*"})
        self.response.payload = service_response
