# zato: ide-deploy=True

from datetime import datetime
from json import dumps

from zato.server.service import Service


class GetStatusLivraison(Service):

    class SimpleIO:
        input_required = ('pk',)

    def handle(self):
        self.logger.info('CID: %s' % self.cid)
        self.logger.info('Request: %s' % self.request.payload)
        self.logger.info('Request type: %s' % type(self.request.payload))
        self.logger.info('Request input: %s' % self.request.input)
        service_response = {}

        try:
            pk = self.request.input.get('pk')
            params = {
                'pk': pk
            }
            erp_get_status_livraison = self.outgoing.plain_http.get('ERP-Livraison')
            response = erp_get_status_livraison.conn.get(self.cid, params)
            self.logger.info('ERP connexion : %s' % response)
            self.logger.info('ERP connexion dict : %s' % response.__dict__)
            self.logger.info('ERP connexion text : %s' % response.text)
            service_response = response.text
        except Exception as e:
            self.logger.info('Exception - ERP GET connexion : %s' % e)

        self.response.headers.update({"Access-Control-Allow-Origin": "*"})
        self.response.payload = service_response
