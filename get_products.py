# zato: ide-deploy=True

from datetime import datetime
from json import dumps, loads

from zato.server.service import Service


class GetProducts(Service):

    def handle(self):
        self.logger.info('Request: %s' % self.request.payload)
        self.logger.info('Request type: %s' % type(self.request.payload))
        service_response = {'data': []}

        try:
            erp_get_products = self.outgoing.plain_http.get('ERP-Produit')
            response = erp_get_products.conn.get(self.cid)
            data = loads(response.text)['data']

            for product in data:
                if product['Achetable']:
                    product = {
                        'id': product['id'],
                        'name': product['Nom'],
                        'list_price': product['Prix-de-vente'],
                        'type': product['Type']
                    }
                    service_response['data'].append(product)
            self.logger.info('ERP connexion : %s' % response)
            self.logger.info('ERP connexion dict : %s' % response.__dict__)
            self.logger.info('ERP connexion text : %s' % response.text)
            service_response = dumps(service_response)
        except Exception as e:
            self.logger.info('Exception - ERP GET connexion : %s' % e)

        self.response.headers.update({"Access-Control-Allow-Origin": "*"})
        self.response.payload = service_response
